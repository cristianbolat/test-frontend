import { InvoiceAppSpaPage } from './app.po';

describe('invoice-app-spa App', () => {
  let page: InvoiceAppSpaPage;

  beforeEach(() => {
    page = new InvoiceAppSpaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
