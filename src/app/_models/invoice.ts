export class Invoice {
    amount: string; 
    created_at: string;
    customer_id: string;
    date: string;
    due_date: string;
    id: string;
    invoice_number: string;
    updated_at: string;
}