﻿import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AuthenticationService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    customers;

    constructor(private http: Http, private authenticationService: AuthenticationService) { }

    ngOnInit() {

    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });

    this.http.get('http://localhost:3000/customers/', options)
      .subscribe(res => this.customers = res.json());
    }

}