import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Invoice } from '../_models/index';
import { AuthenticationService } from '../_services/index';

import { ActivatedRoute } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit, OnDestroy {
  invoices : Invoice[];
  invoicesList : Invoice[];
  selectedInvoice : Invoice;
  isNew : boolean = false;

  id: number;
  private sub: any;

  constructor(private http: Http, private authenticationService: AuthenticationService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.loadInvoices();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  performSearch(searchTerm: string): void {
    if (searchTerm) {
      var filteredInvoices : Invoice[] = [];
      for(let inv of this.invoicesList) {
        for (var key in inv) {
          if (JSON.stringify(inv[key]).toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0) {
            filteredInvoices.push(inv);
            break;
          }
        }
      }

      this.invoices = filteredInvoices;

    } else {
      this.invoices = this.invoicesList;
    }
  }

  onSelect(invoice: Invoice): void { 
    this.selectedInvoice = invoice;
  }

  createIvoice() {
    this.selectedInvoice = new Invoice();
    this.isNew = true;
  }

  save() {
    if (this.isNew) {
      let headers = new Headers({ 'Authorization': this.authenticationService.token });

      let data = {
          "invoice_number": this.selectedInvoice.invoice_number.toString(),
          "date": this.selectedInvoice.date.toString(),
          "due_date": this.selectedInvoice.due_date.toString(),
          "amount": this.selectedInvoice.amount.toString()
      };

      var url = 'http://localhost:3000/customers/' + this.id + '/invoices/';

      this.http.post(url, data, { headers: headers })
        .subscribe(data => { 
                  console.log('ok');
                  }, error => {
                      console.log(JSON.stringify(error.json()));
                  });

    } else {
      
      let headers = new Headers({ 'Authorization': this.authenticationService.token });

      let data = {
          "invoice_number": this.selectedInvoice.invoice_number.toString(),
          "date": this.selectedInvoice.date.toString(),
          "due_date": this.selectedInvoice.due_date.toString(),
          "amount": this.selectedInvoice.amount.toString()
      };

      var url = 'http://localhost:3000/customers/' + this.id + '/invoices/' + this.selectedInvoice.id;

      this.http.put(url, data, { headers: headers })
        .subscribe(data => {
                    console.log('ok');
                  }, error => {
                    console.log(JSON.stringify(error.json()));
                  });
    }

    this.selectedInvoice = null;
    this.isNew = false;

    // yes I am acutely aware this is not the desired solution
    window.location.reload();
  }

  cancel() {
    this.selectedInvoice = null;
    this.isNew = false;
  }

  loadInvoices() {
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });

    var url = 'http://localhost:3000/customers/' + this.id + '/invoices';

    this.http.get(url, options)
      .subscribe(res => this.invoices = this.invoicesList = res.json());
  }
}
