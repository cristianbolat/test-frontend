import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services/authentication.service';

@Component({
  moduleId: module.id,
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  register() {
    this.authenticationService.register(this.model.name, this.model.email, this.model.password, this.model.confirmPassword)
      .subscribe(result => {
        if (result === true) {
            this.router.navigate(['/home']);
            console.log('Registration successfull!');
        }
      });
    }

}
